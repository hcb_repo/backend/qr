package com.sprintsquads.qr.controller;

import com.sprintsquads.qr.model.QrData;
import com.sprintsquads.qr.model.QrDataDto;
import com.sprintsquads.qr.service.QrService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController()
@RequestMapping("/")
@RequiredArgsConstructor
public class QrController {

    private final QrService qrService;

    @PostMapping(value = "/generate", produces = MediaType.IMAGE_PNG_VALUE)
    public byte[] generateQr(@RequestBody QrDataDto qrDataDto) {
        try {
            return qrService.generateQr(qrDataDto);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
