package com.sprintsquads.qr.service;

import com.sprintsquads.qr.model.QrData;

public interface QrDataService {

    QrData findById(String id);
}
