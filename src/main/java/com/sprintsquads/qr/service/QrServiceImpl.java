package com.sprintsquads.qr.service;

import com.google.zxing.WriterException;
import com.sprintsquads.qr.model.QrData;
import com.sprintsquads.qr.model.QrDataDto;
import com.sprintsquads.qr.repository.QrDataRepository;
import com.sprintsquads.qr.utils.QrUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

@Service
@RequiredArgsConstructor
public class QrServiceImpl implements QrService{

    private final QrDataRepository qrDataRepository;


    @Override
    public byte[] generateQr(QrDataDto qrDataDto) throws WriterException, IOException {

        QrData qrData = qrDataRepository.findQrData(qrDataDto.getMerchantId(), qrDataDto.getArticulId(), qrDataDto.getPrice(), qrDataDto.getPointOfSaleId());

        if (qrData == null) {
            qrData = new QrData();
            qrData.setArticulId(qrDataDto.getArticulId());
            qrData.setMerchantId(qrDataDto.getMerchantId());
            qrData.setPrice(qrDataDto.getPrice());
            qrData.setPointOfSaleId(qrDataDto.getPointOfSaleId());
            qrDataRepository.save(qrData);
        }

        BufferedImage img = QrUtils.generateEAN13BarcodeImage(qrData.getId().toString());
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(img, "jpg", baos);
        return baos.toByteArray();
    }


}
