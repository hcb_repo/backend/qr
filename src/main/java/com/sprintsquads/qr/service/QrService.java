package com.sprintsquads.qr.service;

import com.google.zxing.WriterException;
import com.sprintsquads.qr.model.QrData;
import com.sprintsquads.qr.model.QrDataDto;

import java.io.IOException;

public interface QrService {

    byte[] generateQr(QrDataDto qrDataDto) throws WriterException, IOException;
}
