package com.sprintsquads.qr.service;

import com.sprintsquads.qr.model.QrData;
import com.sprintsquads.qr.repository.QrDataRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class QrDataServiceImpl implements QrDataService{

    private final QrDataRepository qrDataRepository;

    @Override
    public QrData findById(String id) {
        return qrDataRepository.findById(id).orElse(null);
    }
}
