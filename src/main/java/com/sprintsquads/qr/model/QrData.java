package com.sprintsquads.qr.model;

import com.google.gson.Gson;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.UUID;

@Data
@Entity
public class QrData {

    @Id
    @GeneratedValue
    private UUID id;

    private String merchantId;
    private String articulId;
    private Float price;
    private Long pointOfSaleId;

    @Override
    public String toString() {
        Gson gson = new Gson();
        return gson.toJson(this, QrData.class);
    }
}
