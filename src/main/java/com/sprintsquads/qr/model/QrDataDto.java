package com.sprintsquads.qr.model;

import lombok.Data;

@Data
public class QrDataDto {

    private String merchantId;
    private Long pointOfSaleId;
    private String articulId;
    private Float price;
}
