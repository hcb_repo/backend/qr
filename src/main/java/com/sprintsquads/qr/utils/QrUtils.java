package com.sprintsquads.qr.utils;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.oned.EAN13Writer;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class QrUtils {

    private static int SIZE = 500;

    public static BufferedImage generateEAN13BarcodeImage(String barcodeText) throws WriterException {
        QRCodeWriter barcodeWriter = new QRCodeWriter();
        BitMatrix bitMatrix = barcodeWriter.encode(barcodeText, BarcodeFormat.QR_CODE, SIZE, SIZE, getSettings());


        return MatrixToImageWriter.toBufferedImage(bitMatrix);
    }


    public static Map<EncodeHintType, Object> getSettings(){
        Map<EncodeHintType, Object> hintMap = new HashMap<>();
        hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.Q);
        hintMap.put(EncodeHintType.QR_VERSION, 2);
        return hintMap;
    }

//    public BufferedImage getQRCodeWithOverlay(BufferedImage qrcode) {
//        BufferedImage scaledOverlay = scaleOverlay(qrcode);
//
//        Integer deltaHeight = qrcode.getHeight() - scaledOverlay.getHeight();
//        Integer deltaWidth  = qrcode.getWidth()  - scaledOverlay.getWidth();
//
//        BufferedImage combined = new BufferedImage(qrcode.getWidth(), qrcode.getHeight(), BufferedImage.TYPE_INT_ARGB);
//        Graphics2D g2 = (Graphics2D)combined.getGraphics();
//        g2.drawImage(qrcode, 0, 0, null);
//        g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, overlayTransparency));
//        g2.drawImage(scaledOverlay, Math.round(deltaWidth/2), Math.round(deltaHeight/2), null);
//        return combined;
//    }
//
//    private BufferedImage scaleOverlay(BufferedImage qrcode)
//    {
//        Integer scaledWidth = Math.round(qrcode.getWidth() * overlayToQRCodeRatio);
//        Integer scaledHeight = Math.round(qrcode.getHeight() * overlayToQRCodeRatio);
//
//        BufferedImage imageBuff = new BufferedImage(scaledWidth, scaledHeight, BufferedImage.TYPE_INT_ARGB);
//        Graphics g = imageBuff.createGraphics();
//        g.drawImage(overlay.getScaledInstance(scaledWidth, scaledHeight, BufferedImage.SCALE_SMOOTH), 0, 0, new Color(0,0,0), null);
//        g.dispose();
//        return imageBuff;
//    }
}
