package com.sprintsquads.qr.repository;

import com.sprintsquads.qr.model.QrData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface QrDataRepository extends JpaRepository<QrData, String> {

    @Query("SELECT qr FROM QrData qr WHERE qr.merchantId = :merchantId AND qr.articulId = :articulId AND qr.price = :price AND qr.pointOfSaleId = :pointOfSaleId")
    QrData findQrData(String merchantId, String articulId, Float price, Long pointOfSaleId);
}
